# Go-CLI

In this project, we are implementing a command line interface (cli) with Go language.
This interface lets us interact with a lightweight database and supports various operations to the user.