package main

import (
	"os"
	"flag"
	"fmt"
	"database/sql"
	_"github.com/go-sql-driver/mysql"
)

type Query interface{
	setRecord()
	printRecord()
}

type Record struct{
	id int
	source_longitude float32
	source_latitude float32
	destination_longitude float32
	destination_latitude float32
	source_address string
	destination_address string
}

type Arguments struct{
	id int
	sLo, sLa, dLo, dLa float32
	s_addr, d_addr string
}
func setRecord(args Arguments) Record {
	return Record{
		id: args.id,
		source_longitude: args.sLo,
		source_latitude: args.sLa,
		destination_longitude: args.dLo, 
		destination_latitude: args.dLa,
		source_address: args.s_addr,
		destination_address: args.d_addr}
}

func printRecord(args Arguments) {
	fmt.Println(args.id, args.sLo, args.sLa, args.s_addr, args.dLo, args.dLa, args.d_addr)
}
func main(){
	var db, err = sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/beatRoutes")

	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var tablePtr = flag.String("table", "routes", "table to ask for queries")
	var columnPtr = flag.String("columns","*", "columns to SELECT query, separated by comma ','")
	var filePtr = flag.String("out-file", "query.out", "path to output file")
	flag.Parse()


	var table_name string = *tablePtr
	var cols string = *columnPtr
	var fl string = *filePtr 

	var f, error = os.Create(fl)
	if error != nil{
		panic(error.Error())
	}
	defer f.Close()



	var rows, err_ = db.Query("SELECT " + cols + " FROM " + table_name)

	if err_ != nil {
	panic(err_.Error())
	}

	var columns, _ = rows.Columns()
	for _, c := range columns{
		f.WriteString(c + "\t")
	}
	f.WriteString("\n")
	var numCols int = len(columns)
	var data_slice = make([]interface{}, numCols)
	var data_slice_refs = make([]interface{}, numCols)

	for rows.Next(){
		// take references of data
		for i, _ := range data_slice{
			data_slice_refs[i] = &data_slice[i]
		}
		// scan function needs pass-by-reference
		rows.Scan(data_slice_refs...)
		for i, data := range data_slice{
			// convert ascii format of data to string format to write to file 
			var value interface{} = string(data.([] byte))
			f.WriteString(string(data.([] byte))+"\t") 
			data_slice[i] = value
		}
		f.WriteString("\n")
		//fmt.Println(data_slice[])
	} 

}